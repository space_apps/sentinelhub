import argparse
import psycopg2


parser = argparse.ArgumentParser()


parser.add_argument("-b", "-blokkazon",
    help="Blokkazonosító, pl: N0A71-X-4A")

"""
parser.add_argument("-d", "date", nargs='+',
    help="Format: YYYY-MM-DD, pl: 2018-12-01, lehet több is")
"""

args = parser.parse_args()


conn = psycopg2.connect(dbname='postgres', user='postgres', password='postgres')

cur = conn.cursor()

query_base = "select ST_AsText(geom) from mepar_blokkok where blokkazon = '{0}'"

def blokk2hub(blokkazon=args.b):
    query = query_base.format(blokkazon)

    try:
        cur.execute(query)

    except psycopg2.Error as e:
        print(e.pgerror)

    result = cur.fetchall()

    result = result[0][0].split(',')

    result[0] = result[0][9:]
    result[-1] = result[-1][:-2]

    result_stripped = []

    for point in result:
        result_stripped.append([point])

    return [result_stripped]

def main():
    blokk2hub()

if __name__ == '__main__':
    main()